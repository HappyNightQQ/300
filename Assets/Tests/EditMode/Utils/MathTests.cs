using System.Collections;
using System.Collections.Generic;
// using NUnit.Framework;
using UnityEngine;
using Utils;
using Assert = UnityEngine.Assertions.Assert;

public class MathTests
{
    // [Test]
    public void PozPozFloatsComparison()
    {
        Assert.IsTrue(MyMath.CompareWithEpsilon(10.5f, 10.5f, 0.01f));
    }
    
    // [Test]
    public void PozNegFloatsComparison()
    {
        Assert.IsFalse(MyMath.CompareWithEpsilon(10.5f, -10.5f, 0.01f));
    }
    
    // [Test]
    public void NegPozFloatsComparison()
    {
        Assert.IsFalse(MyMath.CompareWithEpsilon(-10.5f, 10.5f, 0.01f));
    }
 
    // [Test]
    public void NegNegFloatsComparison()
    {
        Assert.IsTrue(MyMath.CompareWithEpsilon(-10.5f, -10.5f, 0.01f));
    }
    
    // [Test]
    public void PozIntNormalize()
    {
        Assert.IsTrue(MyMath.Normalize(430635.8687f) == 1);
    }
    
    // [Test]
    public void NegIntNormalize()
    {
        Assert.IsTrue(MyMath.Normalize(-30635.5648f) == -1);
    }
    
    // [Test]
    public void ZeroIntNormalize()
    {
        Assert.IsTrue(MyMath.Normalize(0.0000000f) == 0);
    }
    
}
