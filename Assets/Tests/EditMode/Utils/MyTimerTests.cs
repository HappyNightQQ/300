using System.Collections;
using System.Collections.Generic;
// using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Utils;
using Assert = UnityEngine.Assertions.Assert;

public class MyTimerTests
{
    // A Test behaves as an ordinary method
    // [Test]
    public void TimerSetsRightTime()
    {
        // Use the Assert class to test conditions
        MyTimer timer = new MyTimer();
        timer.Start(2f);
        Assert.IsTrue(MyMath.CompareWithEpsilon(timer.TimeLeft, 2f, 0.01f));
    }

    // [Test]
    public void TimerHasStarted()
    {
        MyTimer timer = new MyTimer();
        timer.Start(2f);
        Assert.IsTrue(timer.HasStarted, "timer.HasStarted");
        Assert.IsTrue(timer.IsTicking , "timer.IsTicking");
        Assert.IsFalse(timer.IsDone, "timer.IsDone");
    }
    
    // [Test]
    public void TimerHasPaused()
    {
        MyTimer timer = new MyTimer();
        timer.Start(2f);

        timer.Pause();
        Assert.IsTrue(timer.HasStarted, "timer.HasStarted");
        Assert.IsFalse(timer.IsTicking, "timer.IsTicking");
        Assert.IsFalse(timer.IsDone, "timer.IsDone");
    }
    
    // [Test]
    public void TimerHasResumed()
    {
        MyTimer timer = new MyTimer();
        timer.Start(2f);
        timer.Pause();
        timer.Resume();
        Assert.IsTrue(timer.HasStarted, "timer.HasStarted");
        Assert.IsTrue(timer.IsTicking,"timer.IsTicking");
        Assert.IsFalse(timer.IsDone, "timer.IsDone");
    }
    
    // [Test]
    public void TimerHasStopped()
    {
        MyTimer timer = new MyTimer();
        timer.Start(2f);
        timer.Stop();
        Assert.IsFalse(timer.HasStarted, "timer.HasStarted");
        Assert.IsFalse(timer.IsTicking, "timer.IsTicking");
        Assert.IsFalse(timer.IsDone, "timer.IsDone");
    }

    // [Test]
    public void TimerTimeTickedManuallyNormalSpeed()
    {
        MyTimer timer = new MyTimer();
        timer.Start(2f);
        timer.Tick(2);
        Assert.IsTrue(timer.IsDone);
    }
    
    // [Test]
    public void TimerTimeTickedManually2XSpeed()
    {
        MyTimer timer = new MyTimer();
        timer.Start(2f);
        timer.Tick(1, 2);
        Assert.IsTrue(timer.IsDone);
    }
    
    // [Test]
    public void TimerTimeTickedManually05XSpeed()
    {
        MyTimer timer = new MyTimer();
        timer.Start(2f);
        timer.Tick(4, 0.5f);
        Assert.IsTrue(timer.IsDone);
    }

    // [TearDown] 
    public void AfterEveryTest()
    {
        
    }

    // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // `yield return null;` to skip a frame.
    // [UnityTest]
    // public IEnumerator TimerTestsWithEnumeratorPasses()
    // {
    //     // Use the Assert class to test conditions.
    //     // Use yield to skip a frame.
    //     yield return null;
    // }
}
