public interface IUpdateable
{
    void MyUpdate();
    bool ShouldUpdate();
}