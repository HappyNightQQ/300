using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Utils;


public class UpdateManager : MonoBehaviourSingleton<UpdateManager>
{
    private HashSet<IUpdateable> _objects;

    protected override void Awake()
    {
        base.Awake();
        _objects = new HashSet<IUpdateable>();
    }

    public void Subscribe(IUpdateable obj)
    {
        _objects.Add(obj);
    }

    //Potential issue? Removing the first one of type. Not the exact one
    public void Unsubscribe(IUpdateable obj)
    {
        _objects.Remove(obj);
    }
    
    void Start()
    {
        _objects ??= new HashSet<IUpdateable>();
    }

    void Update()
    {
        foreach (var updateable in _objects)
        {
            updateable.MyUpdate();
        }
    }
}