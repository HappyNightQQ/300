using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameConstants
{
    public const int MaxPopulation = 40;
    public const float MinControllerDeadZone = 0.15f;
    public const float MaxControllerDeadZone = 0.85f;
    public const float AfkTimer = 5.0f;
}
