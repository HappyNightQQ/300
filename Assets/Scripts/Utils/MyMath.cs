using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Utils
{
    public static class MyMath
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool CompareWithEpsilon(float a, float b, float epsilon)
        {
            return Mathf.Abs(b - a) < epsilon;
        }

        public static int Normalize(float a)
        {
            if (a > 0)
                return 1;

            if (a < 0)
                return -1;
            
            return 0;
        }
    }   
}
