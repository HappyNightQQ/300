using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

namespace Utils
{
    public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance;

        public static T Instance
        {
            get
            {
                if (null == instance)
                {
                    instance = FindObjectOfType<T>();
                    if (null == instance)
                    {
                        var obj = new GameObject
                        {
                            name = typeof(T).Name + " (Always Available Singleton)"
                        };
                        instance = obj.AddComponent<T>();
                    }
                }

                return instance;
            }
        }

        protected virtual void Awake()
        {
            if (null != instance) return;
            instance = this as T;
            DontDestroyOnLoad(this.gameObject);
        }
    }
}