using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Rendering;

public class ScriptableObjectSingleton<T> : ScriptableObject where T : ScriptableObjectSingleton<T>
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = Resources.FindObjectsOfTypeAll(typeof(T)).FirstOrDefault() as T;
            }

#if UNITY_EDITOR
            if (_instance == null)
            {
                Debug.Assert(false, $"AlwaysAvailableScriptableObjectSingleton of type {typeof(T)} has no instance");
            }
#endif
            
            return _instance;
        }
    }
}
