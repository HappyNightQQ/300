using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public class MyTimer
    {
        public bool IsDone { get; private set; } = false;
        public bool HasStarted { get; private set; } = false;
        public bool IsTicking { get; private set; } = false;
        public float TimeLeft { get; private set; } = 0;

        public delegate void StateChangedDelegate();
        public StateChangedDelegate onDone;
        public StateChangedDelegate onStart;
        public StateChangedDelegate onPause;
        public StateChangedDelegate onStop;
        public StateChangedDelegate onResume;

        /*
         * 1 is 100%, 2 is 200%, 2.34 is 234%
         */
        public void Tick(float deltaTime, float speed = 1)
        {
            if (!IsTicking) return;
            
            TimeLeft -= deltaTime * speed;
            if (TimeLeft > 0) return;
        
            IsDone = true;
            onDone?.Invoke();
            Stop();
        }

        public void Start(float secondsToWait)
        {
            Debug.Log("Start time is " + Time.time);
            _initialSeconds = TimeLeft = secondsToWait;
            IsTicking = true;
            HasStarted = true;
            IsDone = false;
            onStart?.Invoke();
        }

        public void Stop()
        {
            Debug.Log("Stop time is " + Time.time);
            Debug.Assert(HasStarted, "Why stop timer without start");
            TimeLeft = 0;
            IsTicking = false;
            HasStarted = false;
            onStop?.Invoke();
        }

        public void Pause()
        {
            Debug.Log("Pause time is " + Time.time);
            IsTicking = false;
            onPause?.Invoke();
        }

        public void Resume()
        {
            Debug.Log("Resume time is " + Time.time);
            IsTicking = true;
            onResume?.Invoke();
        }

        public void ReInit()
        {
            HasStarted = false;
            IsDone = false;
            TimeLeft = 0;
        }

        public void Reset()
        {
            TimeLeft = _initialSeconds;
        }

        public void AddTime(float seconds)
        {
            if (!HasStarted)
            {
                Start(seconds);
            }

            TimeLeft += seconds;
        }

        public void Remove(float seconds)
        {
            if (!HasStarted) return;

            TimeLeft -= seconds;
        }

        private float _initialSeconds;
    }
}
