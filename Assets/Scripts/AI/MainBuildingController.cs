using System.Collections;
using System.Collections.Generic;
using AIState;
using UnityEngine;
using UnityEngine.Events;

namespace AIState
{
    public class MainBuildingController : BuildingController
    {
        public delegate void OnKeyPressedDelegateNoArgs();
        public OnKeyPressedDelegateNoArgs onMainBuildingUpgraded;

        public override void MyUpdate()
        {
            if (IsDestroyed())
            {
                Debug.Log("Game Over");
            }
            base.MyUpdate();
        }
        
        public override void OnUpgradeDone()
        {
            base.OnUpgradeDone();
            onMainBuildingUpgraded();
        }
    }
   
}