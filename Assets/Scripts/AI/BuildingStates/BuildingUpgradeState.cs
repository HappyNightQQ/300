using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AIState;
using PlayerLogic;
using UnityEngine;
using Utils;

public class BuildingUpgradeState : BuildingBaseState
{
    public BuildingUpgradeState(BuildingController manager) : base(manager) { }

    public override void StateEnter()
    {
        AddWorker();
    }

    public override void StateUpdate()
    {
        if (manager.IsDestroyed()) 
            manager.ChangeState(new BuildingIdleState(manager));

        if (manager.actionTimer.IsDone)
        {
            OnUpgradeDone();
            return;
        }
        
        manager.actionTimer.Tick(Time.deltaTime, _upgradeSpeed);
    }
    
    private void OnUpgradeDone()
    {
        manager.OnUpgradeDone();
        _nrOfBuilders = 0;
        _upgradeSpeed = 0;
        manager.ChangeState(new BuildingIdleState(manager));
    }
    
    public override void StateExit() { }

    public override void AddWorker()
    {
        ++_nrOfBuilders;
        _upgradeSpeed = manager.GetUpgradeSpeed();
        if (manager.actionTimer.IsTicking) return;
        
        if (manager.actionTimer.HasStarted)
        {
            manager.actionTimer.Resume();
        }
        else
        {
            manager.actionTimer.Start(manager.GetUpgradeTime());
            if (manager.IsDestroyed())
            {
                Debug.Log("Repair timer started at {0}" + Time.time + "and for"+ manager.GetUpgradeTime() + "sec");
            }
            else
            {
                Debug.Log("Upgrade timer started at {0}" + Time.time + "and for"+ manager.GetUpgradeTime() + "sec");
            }
        }
    }
    
    public override void RemoveWorker()
    {
        --_nrOfBuilders;
        _upgradeSpeed = manager.GetUpgradeSpeed(); 
        
        if (_nrOfBuilders != 0) return;
        manager.actionTimer.Pause();
    }

    public override bool IsUpgrading()
    {
        return true;
    }

    private int _nrOfBuilders = 0;
    private float _upgradeSpeed = 0f;
}
