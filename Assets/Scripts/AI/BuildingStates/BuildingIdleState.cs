using System.Collections;
using System.Collections.Generic;
using PlayerLogic;
using UnityEngine;

namespace AIState
{
    public class BuildingIdleState : BuildingBaseState
    {
        public BuildingIdleState(BuildingController manager) : base(manager)
        {
        }
        
        public override void StateEnter()
        {
        }

        public override void StateExit()
        {
        }

        public override void StateUpdate()
        {
            switch (manager.buildingData.type)
            {
                case BuildingTypes.CityCenter:
                    break;
                case BuildingTypes.ArcherHut:
                case BuildingTypes.BuildersHut:
                case BuildingTypes.FarmerHut:
                case BuildingTypes.SpearmanHut:
                    if (manager.CanSpawn())
                        manager.ChangeState(new BuildingSpawnState(manager));
                    break;
                case BuildingTypes.Wall:
                    if (manager.CanDefend()) 
                        manager.ChangeState(new BuildingDefenseState(manager));
                    break;
                case BuildingTypes.Farm:
                    // manager.ChangeState(new BuildingFarmState(manager));
                    break;
                case BuildingTypes.BerryBush:
                    manager.ChangeState(new BuildingProduceState(manager));
                    break;
            }
        }

        public override void AddWorker()
        {
            manager.ChangeState(new BuildingUpgradeState(manager));
        }
        
        public override void RemoveWorker()
        {
            Debug.LogError("Why remove in building idle state " + this);
        }

        public override bool IsUpgrading()
        {
            return false;
        }
    }
}
