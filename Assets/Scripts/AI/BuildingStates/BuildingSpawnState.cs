using System.Collections;
using System.Collections.Generic;
using PlayerLogic;
using UnityEngine;

namespace AIState
{
    public class BuildingSpawnState : BuildingBaseState
    {
        public BuildingSpawnState(BuildingController manager) : base(manager)
        {
        }
        
        public override void StateEnter()
        {
            manager.actionTimer.ReInit();
            manager.actionTimer.Start(manager.buildingData.TimeBetweenSpawns);
        }

        public override void StateExit()
        {
        }

        public override void StateUpdate()
        {
            if (manager.IsDestroyed()) 
                manager.ChangeState(new BuildingIdleState(manager));
            
            manager.actionTimer.Tick(Time.deltaTime);
            
            if (!manager.actionTimer.IsDone) return;
            
            manager.SpawnWeapon();
            manager.ChangeState(new BuildingIdleState(manager));
        }
        

        public override void AddWorker()
        {
            manager.ChangeState(new BuildingUpgradeState(manager));
        }

        public override void RemoveWorker()
        {
            Debug.LogError("Why remove in building spawn state " + this);
        }
        
        public override bool IsUpgrading()
        {
            return false;
        }
    }
}
