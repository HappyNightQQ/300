using System.Collections;
using System.Collections.Generic;
using AIState;
using PlayerLogic;
using UnityEngine;

public class BuildingDefenseState : BuildingBaseState
{
    public BuildingDefenseState(BuildingController manager) : base(manager)
    {
    }
        
    public override void StateEnter()
    {
    }

    public override void StateExit()
    {
    }

    public override void StateUpdate()
    {
        if (manager.IsDestroyed()) 
            manager.ChangeState(new BuildingIdleState(manager));
    }

    public override void AddWorker()
    {
        manager.ChangeState(new BuildingUpgradeState(manager));
    }
        
    public override void RemoveWorker()
    {
        Debug.LogError("Why remove in building defense state " + this);
    }

    public override bool IsUpgrading()
    {
        return false;
    }
}
