using System.Collections.Generic;
using PlayerLogic;
using UnityEngine;

public class Workers
{
    private List<WorkerData> _workers;
    public bool[] occupiedWorkIndex;
    private BuildingTypes _type;

    public Workers(int length, BuildingTypes type)
    {
        _workers = new List<WorkerData>(length);
        occupiedWorkIndex = new bool[length];
        _type = type;
    }


    public void Add(UnitController unit, int workIndex)
    {
        if (Debug.isDebugBuild)
        {
            if (workIndex >= occupiedWorkIndex.Length)
            {
                Debug.LogError("Build index is out of bounds");
                return;
            }
            
            if (null != _workers.Find(builder => builder.unit == unit))
            {
                Debug.LogError("Why add what is already in");
                return;
            }   
        }

        
        var tmp = new WorkerData(unit, workIndex, _type);
        occupiedWorkIndex[workIndex] = true;
        _workers.Add(tmp);
    }

    public void StartWorking(UnitController unit)
    {
        if (Debug.isDebugBuild)
        {
            if (null == _workers.Find(builder => builder.unit == unit))
            {
                Debug.LogError("StartBuilding but builder is not in vector");
                return;
            }
        }
        _workers.Find(builder => builder.unit == unit).working = true;
    }

    public void Remove(UnitController unit)
    {
        if (Debug.isDebugBuild)
        {
            if (null == _workers.Find(builder => builder.unit == unit))
            {
                Debug.LogError("Remove Builder but not in vector");
                return;
            }
        }

        var builder = _workers.Find(builder => builder.unit == unit);
        occupiedWorkIndex[builder.buildSlotIndex] = false;
        _workers.Remove(builder);
    }

    public float GetWorkSpeed()
    {
        var sum = 0f;
        foreach (var v in _workers)
        {
            if (v.working)
                sum += v.speed;
        }
        
        return sum;
    }

    public void ClearAndFollowPlayerOrIdle(GameObject player)
    {
        var order = PlayerOrders.Orders.FollowOrder;
        if (null == player)
            order = PlayerOrders.Orders.Idle;
        
        while(_workers.Count > 0)
        {
            _workers[0].unit.OnOrder(order, player);
        }
        _workers.Clear();
        for (int i = 0; i < occupiedWorkIndex.Length; i++)
        {
            occupiedWorkIndex[i] = false;
        }
    }

    public void TransferMoney(int amount, GameObject player)
    {
        if (null == _workers || _workers.Count == 0)
        {
            Debug.LogError("TransferMoney : Why TransferMoney if workers is empty");
            return;
        }

        var pl = player.GetComponent<Player>();
        if (null == pl)
        {
            Debug.LogError("TransferMoney : Why TransferMoney if game object isn't a player");
            return;
        }
        pl.AddMoney(amount);
    }
    
    private class WorkerData
    {
        public UnitController unit { get; set; }
        public float speed { get; set; }
        public bool working { get; set; }
        public int buildSlotIndex { get; set; }

        public WorkerData(UnitController unit, int buildSlotIndex, BuildingTypes type)
        {
            this.unit = unit;
            switch (type)
            {
                case BuildingTypes.CityCenter:
                case BuildingTypes.Wall:
                case BuildingTypes.ArcherHut:
                case BuildingTypes.BuildersHut:
                case BuildingTypes.FarmerHut:
                case BuildingTypes.SpearmanHut:
                    speed = unit.unitData.upgradeSpeed;
                    break;
                case BuildingTypes.BerryBush:
                    speed = unit.unitData.gatherSpeed;
                    break;
            }
            this.buildSlotIndex = buildSlotIndex;
            working = false;
        }
    }
}