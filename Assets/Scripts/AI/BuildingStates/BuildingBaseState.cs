using System;
using System.Collections;
using System.Collections.Generic;
using PlayerLogic;
using UnityEngine;

namespace AIState
{
    public abstract class BuildingBaseState
    {
        protected BuildingController manager;
        protected int nbOfSpawnedTools = 0;

        protected BuildingBaseState(BuildingController manager) { this.manager = manager; }

        public abstract void StateEnter();

        public abstract void StateExit();

        public abstract void StateUpdate();

        public abstract void AddWorker();

        public abstract void RemoveWorker();
        
        public abstract bool IsUpgrading();
    }
}