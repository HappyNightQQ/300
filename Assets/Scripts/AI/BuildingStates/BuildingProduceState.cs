using System.Collections;
using System.Collections.Generic;
using AIState;
using PlayerLogic;
using UnityEngine;
using Utils;

public class BuildingProduceState : BuildingBaseState
{
    public BuildingProduceState(BuildingController manager) : base(manager)
    {
        this._berryLvl = manager.level;
        gatherTimer = new MyTimer();
    }
    
    public override void StateEnter()
    {
        manager.actionTimer.Start(manager.buildingData.GetUpgradeTimeFromLevel(_berryLvl));
    }

    public override void StateExit()
    {
    }

    public override void StateUpdate()
    {
        if (_nbOfGatherers == 0)
        {
            if (_berryLvl == manager.buildingData.GetCurrMaxLevelByCityCenter(_berryLvl)) return;
        
            manager.actionTimer.Tick(Time.deltaTime);

            if (!manager.actionTimer.IsDone) return;
        
            ++_berryLvl;
            manager.OnBerryUpgradeDone();
        
            if (_berryLvl == manager.buildingData.GetCurrMaxLevelByCityCenter(_berryLvl)) return;
            manager.actionTimer.Start(manager.buildingData.GetUpgradeTimeFromLevel(_berryLvl));    
        }
        else
        {
            gatherTimer.Tick(Time.deltaTime);
            if (!gatherTimer.IsDone) return;
            OnBerryGatherDone();
        }
    }

    private void OnBerryGatherDone()
    {
        manager.OnBerryGatherDone();
        _berryLvl = 0;
        manager.actionTimer.Start(manager.buildingData.GetUpgradeTimeFromLevel(_berryLvl));
    }

    public override void AddWorker()
    {
        if (_nbOfGatherers == 0)
        {
            manager.actionTimer.Pause();
            if (gatherTimer.HasStarted)
                gatherTimer.Resume();
            else
                gatherTimer.Start(manager.buildingData.GetRepairTimeForLevel(0));            
        }
        ++_nbOfGatherers;
    }
        
    public override void RemoveWorker()
    {
        --_nbOfGatherers;
        if (_nbOfGatherers == 0)
        {
            gatherTimer.Reset();
            gatherTimer.Pause();
            
            if (!manager.actionTimer.IsDone)
                manager.actionTimer.Resume();
        }
    }

    public override bool IsUpgrading()
    {
        return false;
    }

    private int _berryLvl = 0;
    private int _nbOfGatherers = 0;
    private MyTimer gatherTimer;
}