using System.Collections.Generic;
using PlayerLogic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;
using Utils;

namespace AIState 
{
    public class BuildingController : MonoBehaviour, IUpdateable
    {
        public BaseBuildingData buildingData;
        public MyTimer actionTimer;
        public int level => _currLevel;
        [SerializeField] private GameObject[] _lvl;
        
        public void ChangeState(BuildingBaseState nextState)
        {
            _state?.StateExit();

            _state = nextState;

            _state?.StateEnter();
        }

        protected void Awake()
        {
            // _animator = GetComponent<Animator>();
            var tmp = transform.GetChild(0);
            _pickableManager = tmp.GetComponent<Pickable>();
            CheckScriptIsSetUp();
            LoadFromSave();
            _state.StateEnter();
        }

        private void CheckScriptIsSetUp()
        {
            // var length = buildingData.upgradeTimeFromLevel.Length;
            //
            // //Doesn't work
            // _lvl ??= new GameObject[length];
            // for (var i = 0; i < length; ++i)
            // {
            //     if (null != _lvl[i]) continue;
            //     
            //     Debug.Log("Why Lvl" + i + " is null");
            //     _lvl[i] = transform.Find("Lvl" + i).gameObject;
            //     Debug.Assert(_lvl[i] != null, "Why lvl" + i +" is null");
            // }
        }

        private void LoadFromSave()
        {
            _state = new BuildingIdleState(this);
            actionTimer = new MyTimer();
            _health = buildingData.Health;
            
            //Set Correct Sprite from Save
            _currLevel = 0;
            foreach (var lvl in _lvl)
            {
                lvl.SetActive(false);
            }
            _lvl[_currLevel].SetActive(true);
            //

            if (IsDestroyed())
                _paymentLeft = buildingData.GetRepairCostForLevel(_currLevel);
            else
                _paymentLeft = buildingData.GetUpgradeCostFromLevel(_currLevel);
            //Set builders if upgrading
            _workers = new Workers(buildingData.BuildPositionsLength, buildingData.type);
            //
        }

        private void Start()
        {
            _state.StateExit();
        }
        
        public bool CanSpawn()
        {
            //Lvl0 -> building not constructed, timer started means it's upgrading
            return buildingData.IsSpawner() && _pickableManager.GetToolCount() < buildingData.MaxToolCount 
                   && _currLevel >= buildingData.minLvlToSpawn && !actionTimer.HasStarted;
        }

        public virtual void MyUpdate()
        {
            _state.StateUpdate();
        }
        
        public float AddWorkerInQueueAndReturnBuildPosition(float positionX, UnitController unit)
        {
            var minDistance = 99999999f;
            //Pooling
            var buildPositions = buildingData.GetBuildPositions(0);
            var finalBuildPos = transform.position.x + buildingData.GetBuildPositions(0);
            var occupiedBuildIndex = 0;
            for (int i = 0; i < buildingData.BuildPositionsLength; ++i)
            {
                if (_workers.occupiedWorkIndex[i]) continue;
            
                var currBuildPos = transform.position.x + buildingData.GetBuildPositions(i);
                var distanceToBuildPos = Mathf.Abs(currBuildPos - positionX);
                if (distanceToBuildPos < minDistance)
                {
                    minDistance = distanceToBuildPos;
                    finalBuildPos = currBuildPos;
                    occupiedBuildIndex = i;
                }
            }
            
            //Reserve a slot in the building process
            _workers.Add(unit, occupiedBuildIndex);
            //Target for unit
            return finalBuildPos;
        }

        #region Upgrade Behaviour
        public float GetUpgradeSpeed()
        {
            return _workers.GetWorkSpeed();
        }

        public void StartWorking(UnitController unit)
        {
            //unit reached building
            _workers.StartWorking(unit);
            _state.AddWorker();
        }
        
        public void RemoveBuilder(UnitController unit)
        {
            _workers.Remove(unit);
            _state.RemoveWorker();
        }
        
        public virtual void OnUpgradeDone()
        {
            _workers.ClearAndFollowPlayerOrIdle(SearchForPlayer());
            actionTimer.ReInit();
            ++_currLevel;
            if (IsDestroyed())
                _paymentLeft = buildingData.GetRepairCostForLevel(_currLevel);
            else
                _paymentLeft = buildingData.GetUpgradeCostFromLevel(_currLevel);
            
            foreach (var lvl in _lvl)
            {
                lvl.SetActive(false);
            }

            if (Debug.isDebugBuild && _currLevel > _lvl.Length - 1)
            {
                Debug.LogError("OnUpgradeDone : curr level is higher than possible");
                return;
            }
            _lvl[_currLevel].SetActive(true);
        }

        private GameObject SearchForPlayer()
        {
            var objects = new List<Collider2D>();
            var point = transform.position;
            var range = buildingData.playerLookRangeAfterBuild;
            var pointA = new Vector2(point.x - range, point.y);
            var pointB = new Vector2(point.x + range, point.y + 0.5f);
            Physics2D.OverlapArea(pointA, pointB, new ContactFilter2D().NoFilter(), objects);

            foreach (var v in objects)
            {
                if (!v.CompareTag("Player")) continue;
                
                return v.gameObject;
            }

            return null;
        }

        private void OnMainBuildingUpgraded()
        {
            ++_currMainBuildingLvl;
        }

        public int GetCost()
        {
            return _paymentLeft;
        }

        public void ReceiveMoney(int amount)
        {
            _paymentLeft -= amount;
        }
        
        public float GetUpgradeTime()
        {
            if (IsDestroyed())
                return buildingData.GetRepairTimeForLevel(_currLevel);
            else
                return buildingData.GetUpgradeTimeFromLevel(_currLevel);
        }

        public bool CanUpgrade()
        {
            return _currLevel < buildingData.GetCurrMaxLevelByCityCenter(_currMainBuildingLvl);
        }
        #endregion

        #region PickUp Behaviour
        public void SpawnWeapon()
        {
            _pickableManager.SpawnWeapon();
        }
                
        public bool HasPickable()
        {
            return null != _pickableManager && _pickableManager.HasPickable();
        }
        
        public bool IsUpgrading()
        {
            return _state.IsUpgrading();
        }
        #endregion

        #region Defend Behaviour
        public bool IsDestroyed()
        {
            return _health < 1;
        }
        
        public bool CanDefend()
        {
            return buildingData.CanBlockEnemies && !IsUpgrading();
        }

        public void TakeDamage(/*Enemy*/UnitController unit, int dmg)
        {
            _health -= dmg;
            DealDamage();
        }

        public void DealDamage()
        {
            
        }
        #endregion
        
        #region Repair Behaviour
        #endregion
        
        #region Farm Behaviour
        public bool CanFarm()
        {
            return _currLevel > buildingData.minLvlToSpawn;
        }
        #endregion
        
        #region Produce Behaviour
        public bool CanBeGathered()
        {
            return buildingData.CanBeGathered(_currLevel);
        }
        
        public void OnBerryUpgradeDone()
        {
            actionTimer.ReInit();
            ++_currLevel;
            
            foreach (var lvl in _lvl)
            {
                lvl.SetActive(false);
            }

            if (Debug.isDebugBuild && _currLevel > _lvl.Length - 1)
            {
                Debug.LogError("OnUpgradeDone : curr level is higher than possible");
                return;
            }
            _lvl[_currLevel].SetActive(true);
        }

        public void OnBerryGatherDone()
        {
            var player = SearchForPlayer();
            _workers.TransferMoney(buildingData.GetUpgradeCostFromLevel(_currLevel - 1), player);
            _workers.ClearAndFollowPlayerOrIdle(player);
            actionTimer.ReInit();
            _currLevel = 0;
            
            foreach (var lvl in _lvl)
            {
                lvl.SetActive(false);
            }

            if (Debug.isDebugBuild && _currLevel > _lvl.Length - 1)
            {
                Debug.LogError("OnUpgradeDone : curr level is higher than possible");
                return;
            }
            _lvl[_currLevel].SetActive(true);
        }
        #endregion
        protected void OnEnable()
        {
            UpdateManager.Instance.Subscribe(this);
            if (buildingData.type != BuildingTypes.CityCenter || buildingData.type != BuildingTypes.BerryBush || buildingData.type != BuildingTypes.None)
            {
                _cityCenter = GameObject.Find("MainBuilding").GetComponent<MainBuildingController>();
                _cityCenter.onMainBuildingUpgraded += OnMainBuildingUpgraded;   
            }
        }

        protected void OnDisable()
        {
            UpdateManager.Instance.Unsubscribe(this);
            if (buildingData.type != BuildingTypes.CityCenter || buildingData.type != BuildingTypes.BerryBush || buildingData.type != BuildingTypes.None)
                _cityCenter.onMainBuildingUpgraded -= OnMainBuildingUpgraded;
        }

        private int _paymentLeft;
        private MainBuildingController _cityCenter = null;
        private BuildingBaseState _state;
        private int _currLevel;
        private int _currMainBuildingLvl;
        private Pickable _pickableManager;
        private int _health;
        private Workers _workers;

        public bool ShouldUpdate()
        {
            return true;
        }
    }
}