using System;
using AIState;
using PlayerLogic;

public sealed class UnitIdleState : UnitBaseState
{
    public UnitIdleState(UnitController manager) : base(manager) { }

    public override void StateEnter()
    {
    }

    public override void StateUpdate()
    {
        if (manager.order == PlayerOrders.Orders.Idle)
            return;
        
        if (!manager.ReachedTarget())
        {
            manager.ChangeState(new UnitMoveState(manager));
        }
    }
    
    public override void StateExit()
    {
    }
}