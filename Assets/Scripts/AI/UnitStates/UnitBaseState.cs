
public abstract class UnitBaseState
{
    protected UnitController manager;

    protected UnitBaseState(UnitController manager)
    {
        this.manager = manager;
    }

    public abstract void StateUpdate();
    public abstract void StateEnter();
    public abstract void StateExit();
}
