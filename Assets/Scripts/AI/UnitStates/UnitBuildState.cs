using System.Collections;
using System.Collections.Generic;
using AIState;
using UnityEngine;
using Utils;

public sealed class UnitBuildState : UnitBaseState
{
    public UnitBuildState(UnitController manager) : base(manager) { }

    public override void StateEnter()
    {
        manager.animator.SetBool(manager.buildAnimHash, true);
        _building = manager.targetGO.GetComponent<BuildingController>();
        StartBuilding();
    }
    
    private void StartBuilding()
    {
        _building.StartWorking(manager);
    }

    public override void StateUpdate()
    {
        if (!manager.ReachedTarget())
        {
            manager.ChangeState(new UnitMoveState(manager));
        }
    }

    public override void StateExit()
    {
        // _building.RemoveBuilder(manager);
        manager.animator.SetBool(manager.buildAnimHash, false);
    }

    private BuildingController _building;
}
