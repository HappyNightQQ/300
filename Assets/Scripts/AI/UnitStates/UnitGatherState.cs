using System.Collections;
using System.Collections.Generic;
using AIState;
using UnityEngine;

public class UnitGatherState : UnitBaseState
{
    public UnitGatherState(UnitController manager) : base(manager) { }

    public override void StateEnter()
    {
        manager.animator.SetBool(manager.gatherAnimHash, true);
        _bush = manager.targetGO.GetComponent<BuildingController>();
        _bush.StartWorking(manager);
    }
    
    public override void StateUpdate()
    {
        if (!manager.ReachedTarget())
        {
            manager.ChangeState(new UnitMoveState(manager));
            
        }
    }
    
    public override void StateExit()
    {
        manager.animator.SetBool(manager.gatherAnimHash, false);
    }
    
    private BuildingController _bush;
}
