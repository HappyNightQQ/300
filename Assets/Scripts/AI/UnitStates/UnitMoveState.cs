using PlayerLogic;
using UnityEngine;
using Utils;

/// <summary>
/// Maybe the most important state. From here it goes to every state
/// When the unit reaches the target location, it executes the order in a different state
/// </summary>
public sealed class UnitMoveState : UnitBaseState
{
    public UnitMoveState(UnitController manager) : base(manager)
    {
        _velocity = manager.unitData.walkingVelocity;
        _random.InitState((uint)Time.time);
    }

    public override void StateEnter()
    {
        manager.animator.SetBool(manager.moveAnimHash, true);
        manager.IsMoving = true;
    }

    public override void StateUpdate()
    {
        if (manager.ReachedTarget())
        {
            _velocity = 0;
            UnitBaseState tmp;
            switch (manager.order)
            {
                case PlayerOrders.Orders.Idle:
                    manager.ChangeState(new UnitIdleState(manager));
                    break;
                case PlayerOrders.Orders.FollowOrder:
                    manager.ChangeState(new UnitFollowState(manager));
                    break;
                case PlayerOrders.Orders.PickUpOrder: 
                    manager.PickUp();
                    manager.ChangeState(new UnitFollowState(manager));
                    break;
                case PlayerOrders.Orders.ClearOrder:
                    break;
                case PlayerOrders.Orders.BuildOrder:
                    manager.ChangeState(new UnitBuildState(manager));
                    break;
                case PlayerOrders.Orders.AttackOrder:
                    break;
                case PlayerOrders.Orders.BlockOrder:
                    break;
                case PlayerOrders.Orders.DefendOrder:
                    break;
                case PlayerOrders.Orders.FarmOrder:
                    break;
                case PlayerOrders.Orders.GatherOrder:
                    manager.ChangeState(new UnitGatherState(manager));
                    break;
            }
        }
        else
        {
            Move();
        }
    }

    private void Move()
    {
        var newPos = manager.Position;
        
        manager.MovingDirection = MyMath.Normalize(manager.GetPositionByOrder() - newPos.x);
        var shouldWalk = manager.ShouldWalk();
        if (manager.ShouldRun() && shouldWalk)
        {
            if (_velocity < manager.unitData.maxRunningVelocity)
            {
                _velocity += manager.unitData.acceleration * Time.deltaTime;
            }
        }
        else if (shouldWalk)
        {
            if (_velocity > manager.unitData.walkingVelocity)
                _velocity -= manager.unitData.deceleration * Time.deltaTime;
        }
        else
        {
            _velocity = manager.unitData.walkingVelocity;
        }

        newPos.x += manager.MovingDirection * Time.deltaTime * _velocity;
        
        manager.SetPosition(newPos);
    }
    
    public override void StateExit()
    {
        manager.animator.SetBool(manager.moveAnimHash, false);
        manager.IsMoving = false;
    }

    private float _velocity;
    private Unity.Mathematics.Random _random;
}
