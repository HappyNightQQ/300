using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitFollowState : UnitBaseState
{
    public UnitFollowState(UnitController manager) : base(manager) { }

    public override void StateEnter()
    {
        
    }
    
    public override void StateUpdate()
    {
        if (manager.ReachedTarget())
        {
            //TODO:: Anims,sounds and shit like that
        }
        else
        {
            manager.ChangeState(new UnitMoveState(manager));
        }
    }
    
    public override void StateExit()
    {
        
    }
}
