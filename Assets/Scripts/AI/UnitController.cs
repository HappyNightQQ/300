using System;
using System.Collections.Generic;
using System.Xml.Schema;
using AIState;
using PlayerLogic;
using UnityEngine;
using Utils;

public class UnitController : MonoBehaviour, IUpdateable
{
    public BasicUnitData unitData;
    public bool IsMoving { get; set; }
    private int _playerDirection;
    public int PlayerDirection
    {
        get => _playerDirection;
        set
        {
            _playerDirection = value;
            UpdatePositionInArmy();
        }
    }
    private int _posIndexInArmy;
    public int PosIndexInArmy
    {
        get => _posIndexInArmy;
        set
        {
            _posIndexInArmy = value;
            UpdatePositionInArmy();
        }
    }
    public int MovingDirection { get; set; }
    public int LookingDirection { get; set; }
    public bool PlayerMoving { get; set; }
    public bool PlayerRunning { get; set; }

    public bool ShouldRun()
    {
        return PlayerRunning;
    }

    public bool ShouldWalk()
    {
        return PlayerMoving;   
    }
    
    #region Animations
    [HideInInspector] public SpriteRenderer spriteRenderer;
    [HideInInspector] public Animator animator;
    // [HideInInspector] public int idleAnimHash;
    [HideInInspector] public int moveAnimHash;
    [HideInInspector] public int buildAnimHash;
    [HideInInspector] public int attackAnimHash;
    [HideInInspector] public int gatherAnimHash;
    [HideInInspector] public int blockAnimHash;
    #endregion
    [HideInInspector] public BasicUnitData.UnitType type;
    public Vector3 Position => this.transform.position;
    [HideInInspector] public PlayerOrders.Orders order;
    [HideInInspector] public GameObject targetGO;
    [HideInInspector] public float targetPos;

    private float _thinkingTimer;
    public void MyUpdate()
    {
        // if (!(Time.time - _thinkingTimer > 0.2f)) return;

        // _thinkingTimer = Time.time;
        _state.StateUpdate();
        if (IsMoving)
            spriteRenderer.flipX = MovingDirection != 1;
        else
            spriteRenderer.flipX = _playerDirection != 1;
    }

    public void ChangeState(UnitBaseState nextState)
    {
        _state?.StateExit();

        _state = nextState;

        _state?.StateEnter();
    }
    
    public void OnOrder(PlayerOrders.Orders playerOrder, GameObject target)
    {
        targetGO = target;
        var lastOrder = order;
        order = playerOrder;
        StopCurrentOrder(lastOrder);

        switch (playerOrder)
        {
            case PlayerOrders.Orders.Idle:
                targetPos = Position.x;
                ChangeState(new UnitIdleState(this));
                break;
            case PlayerOrders.Orders.FollowOrder:
                var player = target.GetComponent<Player>();
                player.AddUnit(gameObject);
                break;
            case PlayerOrders.Orders.BuildOrder:
            case PlayerOrders.Orders.RepairOrder:
            case PlayerOrders.Orders.GatherOrder:
                _building = target.GetComponent<BuildingController>();
                targetPos = _building.AddWorkerInQueueAndReturnBuildPosition(transform.position.x, this);
                break;
            case PlayerOrders.Orders.PickUpOrder:
                _pickable = target.GetComponent<Pickable>();
                targetPos = _pickable.GetLocation();
                break;
        }
    }

    private void StopCurrentOrder(PlayerOrders.Orders lastOrder)
    {
        switch (lastOrder)
        {
            case PlayerOrders.Orders.BuildOrder:
            case PlayerOrders.Orders.RepairOrder:
            case PlayerOrders.Orders.GatherOrder:
                _building.RemoveBuilder(this);
                break;
        }
    }

    public bool ReachedTarget()
    {
        var targetPosition = GetPositionByOrder();
        // Debug.Log("Target destination is " + targetPosition);
        var marginOfError = 0.01f;
        switch (order)
        {
            case PlayerOrders.Orders.Idle:
                break;
            case PlayerOrders.Orders.FollowOrder:
                // marginOfError = startedMoving ? 0.2f : 0.5f;
                break;
            case PlayerOrders.Orders.PickUpOrder:
            case PlayerOrders.Orders.BuildOrder:
            case PlayerOrders.Orders.ClearOrder:
            case PlayerOrders.Orders.AttackOrder:
            case PlayerOrders.Orders.BlockOrder:
            case PlayerOrders.Orders.DefendOrder:
            case PlayerOrders.Orders.FarmOrder:
            case PlayerOrders.Orders.GatherOrder:
                break;
            default:
                return false;
        }
        
        return MyMath.CompareWithEpsilon(Position.x, targetPosition, marginOfError);
    }
    
    public float GetPositionByOrder()
    {
        switch (order)
        {
            case PlayerOrders.Orders.FollowOrder:
                return targetGO.transform.position.x + _posInArmy;
            case PlayerOrders.Orders.BuildOrder:
            case PlayerOrders.Orders.PickUpOrder:
            case PlayerOrders.Orders.GatherOrder:
                // return _building.AddBuilderAndReturnBuildPosition(transform.position.x, this);
                return targetPos;
            case PlayerOrders.Orders.ClearOrder:
            case PlayerOrders.Orders.AttackOrder:
            case PlayerOrders.Orders.BlockOrder:
            case PlayerOrders.Orders.DefendOrder:
            case PlayerOrders.Orders.FarmOrder:
                return targetGO.transform.position.x;
            case PlayerOrders.Orders.Idle:
            default:
                return Position.x;
        }
    }

    private void UpdatePositionInArmy()
    {
        _posInArmy = unitData.distanceFromPlayer + (unitData.distanceFromFrontUnit * _posIndexInArmy);
        _posInArmy *= -_playerDirection;
    }

    public bool CanExecute(PlayerOrders.Orders newOrder)
    {
        return unitData.CanExecute(newOrder);
    }

    public void SetPosition(Vector3 newPosition)
    {
        transform.position = newPosition;
    }

    private void Awake()
    {
        Init();
        LoadFromSave();
    }

    private void Init()
    {
        type = unitData.GetUnitType();
        targetGO = this.gameObject;
        order = PlayerOrders.Orders.Idle;
        UpdatePositionInArmy();
        InitAnimations();
    }
    
    private void InitAnimations()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        // idleAnimHash = Animator.StringToHash("Idling");
        moveAnimHash = Animator.StringToHash("Walking");
        buildAnimHash = Animator.StringToHash("Building");
        // attackAnimHash = Animator.StringToHash("AttackType");
        // blockAnimHash = Animator.StringToHash("Blocking");
        gatherAnimHash = Animator.StringToHash("Gathering");
    }
    
    private void LoadFromSave()
    {
        _state = new UnitIdleState(this);
    }

    private void OnEnable()
    {
        UpdateManager.Instance.Subscribe(this);
    }
    
    private void OnDisable()
    {
        UpdateManager.Instance.Unsubscribe(this);
    }
    
    public bool ShouldUpdate()
    {
        return true;
    }

    // private void OnGUI()
    // {
    //     string content = _state != null ? _state.ToString() : "no current state";
    //     GUILayout.Label($"<color='black><size=20>{content}</size></color>");
    // }

    private UnitBaseState _state;
    private float _posInArmy;
    private BuildingController _building = null;
    private Pickable _pickable = null;

    public void PickUp()
    {
        _pickable.PickUp();
        Debug.Log("Unit picked up " + _pickable.type);
        switch (_pickable.type)
        {
            case BasicUnitData.UnitType.Archer:
                break;
            case BasicUnitData.UnitType.Builder:
                break;
            case BasicUnitData.UnitType.Farmer:
                break;
            case BasicUnitData.UnitType.Spearman:
                break;
        }
    }

    public void TransferMoneyToPlayer(int amount)
    {
        throw new NotImplementedException();
    }
}