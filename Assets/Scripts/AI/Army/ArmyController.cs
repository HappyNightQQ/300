using System.Collections.Generic;
using PlayerLogic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace ArmyAI
{
    public class ArmyController : List<ArmyController>
    {
        public ArmyController(Player controller)
        {
            _player = controller;
            _army = new Army();
        }

        public void AddFollow(GameObject unitGO)
        {
            var unit = unitGO.GetComponent<UnitController>();
            if (Debug.isDebugBuild && null == unit)
            {
                Debug.LogError("ArmyController.Add : Tried to add non unit");
                return;
            }
            unit.OnOrder(PlayerOrders.Orders.FollowOrder, _player.gameObject);
        }
        
        public void Add(GameObject unitGO)
        {
            var unit = unitGO.GetComponent<UnitController>();
            if (Debug.isDebugBuild && null == unit)
            {
                Debug.LogError("ArmyController.Add : Tried to add non unit");
                return;
            }
            Add(unit);
        }

        public void Add(UnitController unit)
        {
            _army.AddUnit(unit);
        }        

        public void Remove(PlayerOrders.Orders playerOrder, GameObject target)
        {
            Order(playerOrder, target);
        }

        public void Order(PlayerOrders.Orders playerOrder, GameObject target)
        {
            UnitController unit = _army.GetFirstUnitThatCan(playerOrder);
            if (null == unit) return;
            _army.RemoveUnit(unit);
            unit.OnOrder(playerOrder, target);
        }

        public void Rotate()
        {
            _army.Rotate();
        }

        public void UpdatePlayerDirection(int dir)
        {
            _army.UpdatePlayerDirection(dir);
        }
        
        public void UpdateIsPlayerRunning(bool runPressed)
        {
            _army.UpdateIsPlayerRunning(runPressed);
        }
        
        public void UpdateIsPlayerMoving(bool isPlayerMoving)
        {
            _army.UpdateIsPlayerMoving(isPlayerMoving);
        }
        
        public bool CanExecute(PlayerOrders.Orders order)
        {
            return _army.CanExecute(order);
        }

        private Army _army = null;
        private Player _player = null;
    }
}