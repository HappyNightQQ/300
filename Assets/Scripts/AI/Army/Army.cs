using System.Collections.Generic;
using PlayerLogic;

namespace ArmyAI
{
    public class Army
    {
        private readonly List<ArmyGroup> _groups;
    
        public Army()
        {
            _groups = new List<ArmyGroup>();
            var groupCount = (int)BasicUnitData.UnitType.Count;
            for (int i = 0; i < groupCount; i++)
            {
                _groups.Add(new ArmyGroup((BasicUnitData.UnitType)i));
            }
        }

        public void UpdatePlayerDirection(int dir)
        {
            foreach (var gArmyGroup in _groups)
            {
                gArmyGroup.UpdatePlayerDirection(dir);
            }
        }
        
        public void UpdateIsPlayerRunning(bool isPlayerRunning)
        {
            foreach (var gArmyGroup in _groups)
            {
                gArmyGroup.UpdateIsPlayerRunning(isPlayerRunning);
            }
        }
        
        public void UpdateIsPlayerMoving(bool isPlayerMoving)
        {
            foreach (var gArmyGroup in _groups)
                gArmyGroup.UpdateIsPlayerMoving(isPlayerMoving);
        }

        public void AddUnit(UnitController unit)
        {
            foreach (var group in _groups)
            {
                if (@group.type != unit.type) continue;
                @group.Add(unit);
                break;
            }
            
            UpdatePositions();
        }
    
        public void RemoveUnit(UnitController unit)
        {
            if (unit == null) return;
            
            foreach (var group in _groups)
            {
                if (group.type == unit.type)
                {
                    group.Remove(unit);
                    break;
                }
            }
            
            UpdatePositions();
        }
    
        public void Rotate()
        {
            if (_groups.Count < 2)
                return;
            
            ArmyGroup tmp = _groups[0];
            for (int i = 0; i < _groups.Count - 1; i++)
            {
                _groups[i] = _groups[i + 1];
            }
    
            _groups[_groups.Count - 1] = tmp;
            
            UpdatePositions();
        }

        public void UpdatePositions()
        {
            var groupPosition = 0;
            foreach (var group in _groups)
            {
                group.UpdatePositions(groupPosition);
                groupPosition += group.Count;
            }
        }
        
        public UnitController GetFirstUnitThatCan(PlayerOrders.Orders order)
        {
            foreach (var @group in _groups)
            {
                if (@group.CanExecute(order))
                    return @group.GetFirstUnit();
            }

            return null;
        }
        
        public bool HasUnitOfType(BasicUnitData.UnitType type)
        {
            foreach (var @group in _groups)
            {
                if (group.type == type && group.Count > 0)
                    return true;
            }

            return false;
        }

        public UnitController GetFirstUnitOfType(BasicUnitData.UnitType type)
        {
            foreach (var @group in _groups)
            {
                if (group.type == type && group.Count > 0)
                    return group.GetFirstUnit();
            }

            return null;
        }

        public bool CanExecute(PlayerOrders.Orders order)
        {
            foreach (var @group in _groups)
            {
                if (group.CanExecute(order))
                    return true;
            }

            return false;
        }
    }
}
