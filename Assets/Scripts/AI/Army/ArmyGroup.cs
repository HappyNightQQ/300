using System.Collections.Generic;
using PlayerLogic;
using UnityEngine;

namespace ArmyAI
{
    /// <summary>
    /// An army group can have only one type of unit
    /// </summary>
    public class ArmyGroup
    {
        private List<UnitController> _units;
        public readonly BasicUnitData.UnitType type;
        public int Count => _units.Count;
        public ArmyGroup(BasicUnitData.UnitType type)
        {
            _units = new List<UnitController>();
            this.type = type;
        }

        public bool Contains(UnitController unit)
        {
            return _units.Contains(unit);
        }

        public void Add(UnitController unit)
        {
            if (unit.type != type || Contains(unit)) return;

            _units.Add(unit);
        }

        public void Remove(UnitController unit)
        {
            if (unit.type != type)
            {
                Debug.LogError("Why try delete different unit type");
                return;
            }

            if (!Contains(unit))
                return;

            _units.Remove(unit);
        }
        
        public void UpdatePositions(int groupFirstPosInArmy)
        {
            foreach (var unit in _units)
            {
                unit.PosIndexInArmy = groupFirstPosInArmy++;
            }
        }

        public UnitController GetFirstUnit()
        {
            return (_units.Count > 0) ? _units[0] : null;
        }

        public bool CanExecute(PlayerOrders.Orders order)
        {
            return Count > 0 && _units[0].CanExecute(order);
        }

        public void UpdatePlayerDirection(int dir)
        {
            foreach (var unit in _units)
                unit.PlayerDirection = dir;
        }

        public void UpdateIsPlayerRunning(bool isPlayerRunning)
        {
            foreach (var unit in _units)
                unit.PlayerRunning = isPlayerRunning;
        }
        
        public void UpdateIsPlayerMoving(bool isPlayerMoving)
        {
            foreach (var unit in _units)
                unit.PlayerMoving = isPlayerMoving;
        }
    }
}
