using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettingsScriptableObject", menuName = "Scriptable Objects/GameSettings")]
public class GameSettingsScriptableObject : ScriptableObject
{
    public float afkThreshold = 20f;
}
