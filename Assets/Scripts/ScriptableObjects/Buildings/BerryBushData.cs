using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BerryBushScriptableObject", menuName = "Scriptable Objects/Buildings/Berry Bush")]
public class BerryBushData : BaseBuildingData
{
}
