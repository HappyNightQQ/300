using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Serialization;
using UnityEngine;

public class DefenseBuildingData : BaseBuildingData
{
    [SerializeField] private int health = 1;

    public override int Health => health;
    public override bool CanBlockEnemies => true;
}
