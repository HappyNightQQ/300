using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FarmerSpawnerBuildingScriptableObject", menuName = "Scriptable Objects/Buildings/Farmer Spawner")]
public class FarmerBuildingData : SpawnerBuildingData
{
    public override BasicUnitData.UnitType UnitTypeSpawned => BasicUnitData.UnitType.Farmer;
}
