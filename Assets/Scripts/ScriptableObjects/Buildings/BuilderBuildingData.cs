using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BuilderSpawnerBuildingScriptableObject", menuName = "Scriptable Objects/Buildings/Builder Spawner")]
public class BuilderBuildingData : SpawnerBuildingData
{
    public override BasicUnitData.UnitType UnitTypeSpawned => BasicUnitData.UnitType.Builder;
}
