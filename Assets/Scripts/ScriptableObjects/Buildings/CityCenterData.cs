using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CityCenterScriptableObject", menuName = "Scriptable Objects/Buildings/City Center")]
public class CityCenterData : BaseBuildingData
{
    
}
