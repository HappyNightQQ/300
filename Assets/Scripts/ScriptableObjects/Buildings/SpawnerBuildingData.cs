using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBuildingData : BaseBuildingData
{
    [SerializeField] private int maxToolCount = 0;
    [SerializeField] private float timeBetweenSpawns = 10f;

    public override float TimeBetweenSpawns => timeBetweenSpawns;
    public override int MaxToolCount => maxToolCount;

    public override bool IsSpawner()
    {
        return true;
    }
    
    public override bool CanWork()
    {
        return true;
    }
}
