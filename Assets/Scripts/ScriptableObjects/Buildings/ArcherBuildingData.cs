using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ArchersSpawnerBuildingScriptableObject", menuName = "Scriptable Objects/Buildings/Archer Spawner")]
public class ArcherBuildingData : SpawnerBuildingData
{
    public override BasicUnitData.UnitType UnitTypeSpawned => BasicUnitData.UnitType.Archer;
}
