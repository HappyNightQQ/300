using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public enum BuildingTypes
{
    None,
    CityCenter,
    ArcherHut,
    BuildersHut,
    SpearmanHut,
    FarmerHut,
    Wall,
    BerryBush,
    Farm
}
public class BaseBuildingData : ScriptableObject
{
    public BuildingTypes type;
    [SerializeField]private float[] buildPositions;
    public int BuildPositionsLength => buildPositions.Length;
    
    [SerializeField]private float[] upgradeTimeFromLevel;
    public int UpgradeTimeFromLevelLength => upgradeTimeFromLevel.Length;
    [SerializeField]private int[] upgradeCostFromLevel;
    public int UpgradeCostFromLevelLength => upgradeCostFromLevel.Length;

    [SerializeField]private float[] repairTimeForLevel;
    public int RepairTimeForLevelLength => repairTimeForLevel.Length;
    [SerializeField]private int[] repairCostForLevel;
    public int RepairCostForLevelLength => repairCostForLevel.Length;
    
    [SerializeField]private int[] currMaxLevelByCityCenter;
    public int CurrMaxLevelByCityCenterLength => currMaxLevelByCityCenter.Length;
    public int minLvlToSpawn;
    public int playerLookRangeAfterBuild = 2;

    public virtual bool CanWork() { return true; }
    
    #region Spawn Buildings
    public virtual float TimeBetweenSpawns => 0;
    public virtual int MaxToolCount => 0;
    
    public virtual BasicUnitData.UnitType UnitTypeSpawned => BasicUnitData.UnitType.Villager;
    public virtual bool IsSpawner() { return false; }
    #endregion
    
    #region Produce Buildings
    public bool CanBeGathered(int level)
    {
        return level > minLvlToSpawn;
    }
    #endregion
    
    #region Defense Buildings
    public virtual int Health => 1;
    public virtual bool CanBlockEnemies => false;
    #endregion
    
    #region Getters
    public float GetUpgradeTimeFromLevel(int level)
    {
        return SafelyGetAtIndex(upgradeTimeFromLevel, level);
    }
    
    public int GetUpgradeCostFromLevel(int level)
    {
        return SafelyGetAtIndex(upgradeCostFromLevel, level);
    }
    
    public float GetRepairTimeForLevel(int level)
    {
        return SafelyGetAtIndex(repairTimeForLevel, level);
    }
    
    public int GetRepairCostForLevel(int level)
    {
        return SafelyGetAtIndex(repairCostForLevel, level);
    }
    
    public int GetCurrMaxLevelByCityCenter(int level)
    {
        return SafelyGetAtIndex(currMaxLevelByCityCenter, level);
    }
    
    public float GetBuildPositions(int level)
    {
        return SafelyGetAtIndex(buildPositions, level);
    }

    private T SafelyGetAtIndex<T>(IReadOnlyList<T> array, int index)
    {
        if (Debug.isDebugBuild && null == array)
        {
            Debug.LogError("SafelyGetAtIndex : Why access something null");
        }
        
        if (index < 0)
            return array[0];

        var length = array.Count - 1;
        if (index > length)
            return array[length];

        return array[index];
    }
    #endregion
}
