using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpearmanSpawnerBuildingScriptableObject", menuName = "Scriptable Objects/Buildings/Spearman Spawner")]
public class SpearmanBuildingData : SpawnerBuildingData
{
    public override BasicUnitData.UnitType UnitTypeSpawned => BasicUnitData.UnitType.Spearman;
}
