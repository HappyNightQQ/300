using UnityEngine;

[CreateAssetMenu(fileName = "PlayerScriptableObject", menuName = "Scriptable Objects/Player")]
public class PlayerScriptableObject : ScriptableObject
{
    public float acceleration = 4f;
    public float deceleration = -1f;
    public float maxRunningVelocity = 3f;
    public float walkingVelocity = 1f;
    
    public float attackTypeResetTime = 1.5f;
    public int numberOfAttackTypes = 3;
    
    public int summonRange = 4;
}
