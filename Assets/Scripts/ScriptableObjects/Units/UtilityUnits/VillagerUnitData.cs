using PlayerLogic;
using UnityEngine;

[CreateAssetMenu(fileName = "VillagerUnitScriptableObject", menuName = "Scriptable Objects/Unit/Villager")]
public class VillagerUnitData : BasicUnitData
{
    public override bool CanExecute(PlayerOrders.Orders order)
    {
        return order == PlayerOrders.Orders.PickUpOrder ||
               order == PlayerOrders.Orders.GatherOrder;
    }
    
    public override UnitType GetUnitType()
    {
        return UnitType.Villager;
    }
}
