using PlayerLogic;
using UnityEngine;

[CreateAssetMenu(fileName = "FarmerUnitScriptableObject", menuName = "Scriptable Objects/Unit/Farmer")]
public class FarmerUnitData : BasicUnitData
{
    public override bool CanExecute(PlayerOrders.Orders order)
    {
        return order == PlayerOrders.Orders.FarmOrder;
    }
}
