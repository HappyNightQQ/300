using PlayerLogic;
using UnityEngine;

[CreateAssetMenu(fileName = "BuilderUnitScriptableObject", menuName = "Scriptable Objects/Unit/Builder")]
public class BuilderUnitData : BasicUnitData
{
    public override bool CanExecute(PlayerOrders.Orders order)
    {
        return order == PlayerOrders.Orders.BuildOrder;
    }
    
    public override UnitType GetUnitType()
    {
        return UnitType.Builder;
    }
}
