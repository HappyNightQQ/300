using PlayerLogic;
using UnityEngine;

public class BasicUnitData : ScriptableObject
{
    public enum UnitType
    {
        Outsider = 0,
        Villager,
        Builder,
        Archer,
        Spearman,
        Farmer,
        Count
    }

    //Moving
    public float acceleration = 4f;
    //Deceleration should be positive
    public float deceleration = 1f;
    public float maxRunningVelocity = 3f;
    public float walkingVelocity = 1f;
    
    //Idling
    public float maxIdleDistance = 3f;
    public float minIdleDistance = 1f;
    public float minIdleTime = 1f;
    public float maxIdleTime = 5f;

    //Following
    public float distanceFromFrontUnit = 0.4f;
    [Tooltip("If you want it to be different from Unit")]
    public float distanceFromPlayer = 0.5f;

    public float upgradeSpeed;
    public float gatherSpeed;

    public virtual bool CanExecute(PlayerOrders.Orders order)
    {
        return false;
    }

    public virtual UnitType GetUnitType()
    {
        return UnitType.Outsider;
    }
}
