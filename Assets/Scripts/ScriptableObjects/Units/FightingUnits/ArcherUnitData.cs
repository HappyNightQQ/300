using PlayerLogic;
using UnityEngine;

[CreateAssetMenu(fileName = "ArcherUnitScriptableObject", menuName = "Scriptable Objects/Unit/Archer")]
public class ArcherUnitData : RangedUnitData
{
    public override bool CanExecute(PlayerOrders.Orders order)
    {
        return order == PlayerOrders.Orders.AttackOrder || 
               order == PlayerOrders.Orders.DefendOrder;
    }
    
    public override UnitType GetUnitType()
    {
        return UnitType.Archer;
    }
}
