using System.Collections;
using System.Collections.Generic;
using AIState;
using PlayerLogic;
using UnityEngine;

public class Wallet
{
    private int _goldInHand = 100;

    public Wallet(int goldInHand)
    {
        this._goldInHand = goldInHand;
    }

    public bool MakePayment(PlayerOrders.Orders order, GameObject obj)
    {
        switch (order)
        {
            case PlayerOrders.Orders.BuildOrder:
            case PlayerOrders.Orders.RepairOrder:
                var building = obj.GetComponent<BuildingController>();
                var cost = building.GetCost();
                if (_goldInHand > cost)
                {
                    _goldInHand -= cost;
                    building.ReceiveMoney(cost);
                    UITextDebug.Instance.UpdateMoney(_goldInHand);
                    return true;
                }
                break;
        }

        return false;
    }

    public void AddMoney(int amount)
    {
        _goldInHand += amount;
        UITextDebug.Instance.UpdateMoney(_goldInHand);
    }
}