using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;

namespace InputController
{
    public class InputDelegates : MonoBehaviourSingleton<InputDelegates>, GameController.IPlayerActions
    {
        #region InputDelegates
        public delegate void OnKeyPressedDelegateFloatArg(float something);
        public OnKeyPressedDelegateFloatArg onMoveKeyPressed;
        public delegate void OnKeyPressedDelegateBoolArg(bool something);
        public OnKeyPressedDelegateBoolArg onRunKeyPressed;
        public OnKeyPressedDelegateBoolArg onAttackKeyPressed;
        public OnKeyPressedDelegateBoolArg onBlockKeyPressed;
        public OnKeyPressedDelegateBoolArg onKickKeyPressed;
        public delegate void OnKeyPressedDelegateNoArgs();
        public OnKeyPressedDelegateNoArgs onCallKeyPressed;
        public OnKeyPressedDelegateNoArgs onInteractKeyPressed;
        public OnKeyPressedDelegateNoArgs onInteractLongKeyPressed;
        public OnKeyPressedDelegateNoArgs onInteractQuickKeyPressed;
        public delegate void OnKeyPressedDelegate(InputAction.CallbackContext context);
        #endregion

        protected override void Awake()
        {
            base.Awake();
            _gameController = new GameController();
            _gameController.Player.SetCallbacks(this);
        }
        
        private void OnEnable()
        {
            _gameController.Enable();
        }

        private void OnDisable()
        {
            _gameController.Disable();
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            float direction = context.ReadValue<Vector2>().x;
            if (direction < /*GameConstants.MinControllerDeadZone*/0.15f && direction > -/*GameConstants.MinControllerDeadZone*/0.15f)
            {
                direction = 0;
            }
            
            if (direction > /*GameConstants.MaxControllerDeadZone*/0.85f) 
            {
                direction = 1;
            }

            if (direction < -/*GameConstants.MaxControllerDeadZone*/0.85f)
            {
                direction = -1;
            }
            onMoveKeyPressed(direction);
        }
        
        public void OnRun(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                onRunKeyPressed(context.ReadValue<float>() != 0);
        }

        public void OnAttack(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                onAttackKeyPressed(context.ReadValue<float>() != 0);
        }

        public void OnBlock(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                onBlockKeyPressed(context.ReadValue<float>() != 0);
        }

        public void OnKick(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                onKickKeyPressed(context.ReadValue<float>() != 0);
        }

        private Coroutine _buttonHoldCoroutine;
        public void OnInteract(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.started)
            {
                onInteractKeyPressed();
                float seconds = 1f;
                _buttonHoldCoroutine ??= StartCoroutine(CheckKeyHeldForTime(seconds));
            }

            if (context.canceled)
            {
                if (null == _buttonHoldCoroutine) return;
                onInteractQuickKeyPressed();
                
                StopCoroutine(_buttonHoldCoroutine);
                _buttonHoldCoroutine = null;
                
            }
        }
        
        public void OnCall(InputAction.CallbackContext context)
        {
            _afkTimer = Time.time;
            if (context.performed)
                onCallKeyPressed();
        }
        
        private IEnumerator CheckKeyHeldForTime(float seconds)
        {
            var waitSeconds = new WaitForSeconds(seconds);
            yield return waitSeconds;
            
            onInteractLongKeyPressed();
            _buttonHoldCoroutine = null;
        }

        public bool IsAfk()
        {
            return Time.time - _afkTimer > GameSettings.Instance.gameSettingsScriptableObject.afkThreshold;
        }

        private GameController _gameController;
        private float _afkTimer;
    }
}
