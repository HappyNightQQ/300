using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentSimulation : MonoBehaviour
{
    private List<Transform> children = null;
    void Start()
    {

        children = new List<Transform>();
        for (int i = 0; i < transform.childCount; ++i)
        {
            children.Add(transform.GetChild(i));
        }

        velocity = 1f;
    }

    [SerializeField] private int leftBorder = -25;
    [SerializeField] private int rightBorder = 50;
    [SerializeField] private float velocity = 0;
    private bool moveLeft = true;
    void Update()
    {
        foreach (var var in children)
        {
            if (moveLeft)
            {
                var.position -= new Vector3(velocity * Time.deltaTime,0,0);
                if (var.position.x < leftBorder)
                {
                    moveLeft = false;
                }
            }
            else
            {
                var.position += new Vector3(velocity * Time.deltaTime,0,0);
                if (var.position.x > rightBorder)
                {
                    moveLeft = true;
                }
            }
            
        }
    }
}
