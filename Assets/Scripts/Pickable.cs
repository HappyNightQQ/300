using System;
using System.Collections;
using System.Collections.Generic;
using AIState;
using UnityEngine;

public class Pickable : MonoBehaviour
{
    private BuildingController _attachedToBuilding;
    public BasicUnitData.UnitType type { get; private set; }
    [SerializeField] private GameObject[] _spawnedTools;
    private int _nbOfSpawnedTools;

    private void Awake()
    {
        _attachedToBuilding = this.transform.parent.GetComponent<BuildingController>();
        type = _attachedToBuilding.buildingData.UnitTypeSpawned;
        CheckScriptIsSetUp();
        LoadFromSave();
    }

    private void CheckScriptIsSetUp()
    {
        var count = _attachedToBuilding.buildingData.MaxToolCount;
        _spawnedTools ??= new GameObject[count];
        for (var i = 0; i < count; ++i)
        {
            if (null != _spawnedTools[i]) continue;
                
            Debug.Log("Why spawnedTool" + i + " is null");
            _spawnedTools[i] = transform.Find("SpawnedTools").GetChild(i).gameObject;
            Debug.Assert(_spawnedTools[i] != null, "Why spawnedTool" + i +" is null");
        }
    }

    private void LoadFromSave()
    {
        //Get Spawned Weapon State from Save
        _nbOfSpawnedTools = 0;
        for (int i = 0; i < _nbOfSpawnedTools; i++)
        {
            _spawnedTools[i].SetActive(true);
        }
    }
    
    public void SpawnWeapon()
    {
        _spawnedTools[_nbOfSpawnedTools++].SetActive(true);
    }

    public void PickUp()
    { 
        _spawnedTools[--_nbOfSpawnedTools].SetActive(false);
    }
    
    public float GetLocation()
    {
        return _spawnedTools[_nbOfSpawnedTools - 1].transform.position.x;
    }

    public bool HasPickable()
    {
        return _nbOfSpawnedTools > 0;
    }

    public int GetToolCount()
    {
        return _nbOfSpawnedTools;
    }
}
