using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Rigidbody2D rb2D = null;
    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    private bool done = false;
    public Vector2 velocity;
    private void FixedUpdate()
    {
        if (done) return;
        float angle = Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        velocity.y -= 9.8f * Time.fixedDeltaTime;
        transform.position += new Vector3(velocity.x, velocity.y, 0) * Time.fixedDeltaTime;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag($"Ground"))
        {
            done = true;
            Destroy(rb2D);
            Destroy(GetComponent<BoxCollider2D>());
            var secondsTillDeath = 10f;
            Destroy(gameObject, secondsTillDeath);
            Destroy(this);
        }
    }
}
