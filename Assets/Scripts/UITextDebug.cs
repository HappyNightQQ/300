using System.Collections;
using System.Collections.Generic;
using PlayerLogic;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class UITextDebug : MonoBehaviourSingleton<UITextDebug>
{
    private Text _amountNumberText;
    private Player _player; 
    void Start()
    {
        _player = GameObject.FindWithTag("Player").GetComponent<Player>();
        _amountNumberText = GetComponent<Text>();
    }

    public void UpdateMoney(int amount)
    {
        _amountNumberText.text = amount.ToString();
    }
}
