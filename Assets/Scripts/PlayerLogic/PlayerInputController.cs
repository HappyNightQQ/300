using System.Collections;
using ArmyAI;
using InputController;
using UnityEngine;
using Utils;

namespace PlayerLogic
{
    public partial class Player
    {
        [Header("Modify this to control speed")]
        public float velocity = 1f;

        [SerializeField] private PlayerScriptableObject playerData;

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _armyController = new ArmyController(this);
        }

        private void Start()
        {
            LoadFromSave();
            InitAnimations();
        }

        private void LoadFromSave()
        {
            _wallet = new Wallet(0);
            _wallet.AddMoney(999);
            
        }

        private void InitAnimations()
        {
            _animator = GetComponent<Animator>();
            _walkAnimHash = Animator.StringToHash("Walking");
            _runAnimHash = Animator.StringToHash("Running");
            _callVillagerAnimHash = Animator.StringToHash("Call");;
            _attackAnimHash = Animator.StringToHash("AttackType");
            _isAfkHash = Animator.StringToHash("AFK");
            _blockAnimHash = Animator.StringToHash("Blocking");
            _kickAnimHash = Animator.StringToHash("Kick");
        }

        private void Update()
        {
            if (_animator.GetInteger(_attackAnimHash) != 0) return;
            
            if (HandleAfk()) return;
            
            if ((_attackPressed || _kickPressed) && !_runPressed)
            {
                if (_kickPressed)
                {
                    if (_attackResetTimerStarted)
                        StopCoroutine(_attackResetTimer);
                    
                    ResetAttackType();
                }
                else
                {
                    HandleAttack();
                }
            }
            else if (_blockPressed && !_runPressed)
            {
                //TODO::Block behaviour
            }
            else if (_movePressed || MovingDirection == 0)
            {
                HandleMovement();
            }
        }

        private void LateUpdate()
        {
            FlipSpriteIfNecessary();
            
        }

        private bool HandleAfk()
        {
            var isAfk = InputDelegates.Instance.IsAfk();
            _animator.SetBool(_isAfkHash, isAfk);
            return isAfk;
        }

        private void HandleMovement()
        {
            if (_runPressed && MovingDirection != 0)
            {
                if (velocity < playerData.maxRunningVelocity)
                    velocity += playerData.acceleration * Time.deltaTime;
            }
            else if (MovingDirection != 0)
            {
                if (velocity > playerData.walkingVelocity)
                    velocity -= playerData.deceleration * Time.deltaTime;
            }
            else
            {
                velocity = playerData.walkingVelocity;
            }

            transform.position += new Vector3(MovingDirection * Time.deltaTime * velocity, 0, 0);
        }

        private void HandleAttack()
        {
            _animator.SetInteger(_attackAnimHash, _currentAttackType++);

            _currentAttackType = _currentAttackType > playerData.numberOfAttackTypes ? 1 : _currentAttackType;

            if (_attackResetTimerStarted)
                StopCoroutine(_attackResetTimer);

            _attackResetTimer = StartCoroutine(AttackTypeResetTimer());
            _attackResetTimerStarted = true;
        }

        private IEnumerator AttackTypeResetTimer()
        {
            yield return new WaitForSeconds(playerData.attackTypeResetTime);

            ResetAttackType();
        }

        private void ResetAttackType()
        {
            _currentAttackType = 1;
            _attackResetTimerStarted = false;
        }

        private void FlipSpriteIfNecessary()
        {
            _spriteRenderer.flipX = LookingDirection == -1;
        }

        private void OnEnable()
        {
            InputDelegates.Instance.onAttackKeyPressed += OnAttack;
            InputDelegates.Instance.onBlockKeyPressed += OnBlock;
            InputDelegates.Instance.onKickKeyPressed += OnKick;
            InputDelegates.Instance.onMoveKeyPressed += OnMove;
            InputDelegates.Instance.onRunKeyPressed += OnRun;
            InputDelegates.Instance.onCallKeyPressed += OnCallKeyPressed;
            InputDelegates.Instance.onInteractKeyPressed += OnInteractFirstPressed;
            InputDelegates.Instance.onInteractLongKeyPressed += OnInteractLongPress;
            InputDelegates.Instance.onInteractQuickKeyPressed += OnInteractQuickPress;
        }

        private void OnDisable()
        {
            InputDelegates.Instance.onAttackKeyPressed -= OnAttack;
            InputDelegates.Instance.onBlockKeyPressed -= OnBlock;
            InputDelegates.Instance.onKickKeyPressed -= OnKick;
            InputDelegates.Instance.onMoveKeyPressed -= OnMove;
            InputDelegates.Instance.onRunKeyPressed -= OnRun;
            InputDelegates.Instance.onCallKeyPressed -= OnCallKeyPressed;
            InputDelegates.Instance.onInteractKeyPressed -= OnInteractFirstPressed;
            InputDelegates.Instance.onInteractLongKeyPressed -= OnInteractLongPress;
            InputDelegates.Instance.onInteractQuickKeyPressed -= OnInteractQuickPress;
        }

        private void OnMove(float direction)
        {
            MovingDirection = MyMath.Normalize(direction);
            _movePressed = MovingDirection != 0;
            _animator.SetBool(_walkAnimHash, _movePressed);
            _armyController.UpdateIsPlayerMoving(_movePressed);
            
            if (MovingDirection == LookingDirection || !_movePressed) return;
            LookingDirection = MovingDirection;
            _armyController.UpdatePlayerDirection(LookingDirection);

        }

        private void OnAttack(bool pressed)
        {
            _attackPressed = pressed;
        }

        private void OnBlock(bool pressed)
        {
            _blockPressed = pressed;
            _animator.SetBool(_blockAnimHash, pressed);
        }

        private void OnKick(bool pressed)
        {
            _kickPressed = pressed;
            _animator.SetBool(_kickAnimHash, pressed);
        }

        private void OnRun(bool pressed)
        {
            _runPressed = pressed;
            _armyController.UpdateIsPlayerRunning(_runPressed);
            _animator.SetBool(_runAnimHash, pressed);
        }

        #region Input Variables
        private bool _movePressed;
        private bool _runPressed;
        private bool _attackPressed;
        private bool _blockPressed;
        private bool _kickPressed;
        [HideInInspector] public int MovingDirection { get; private set; }= 1;
        [HideInInspector] public int LookingDirection { get; private set; }= 1;
        #endregion

        private SpriteRenderer _spriteRenderer;
        [Header("Attack Control Variables")]
        private int _currentAttackType = 1;
        private Coroutine _attackResetTimer;
        private bool _attackResetTimerStarted;

        #region Animation Variables
        private Animator _animator;
        private int _walkAnimHash;
        private int _callVillagerAnimHash;
        private int _runAnimHash;
        private int _attackAnimHash;
        private int _blockAnimHash;
        private int _kickAnimHash;
        private int _isAfkHash;
        #endregion
    }
}