using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerLogic
{
    public static class PlayerOrders
    {
        public enum Orders
        {
            Idle,
            FollowOrder,
            PickUpOrder,
            RepairOrder,
            ClearOrder,
            GatherOrder,
            BuildOrder,
            AttackOrder,
            BlockOrder,
            DefendOrder,
            FarmOrder,
        }

        public static bool RequiresLongPress(Orders order)
        {
            switch (order)
            {
                case Orders.BuildOrder:
                    return true;
                default:
                    return false;
            }
        }
    }
}