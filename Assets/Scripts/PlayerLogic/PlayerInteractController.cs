using System.Collections;
using System.Collections.Generic;
using AIState;
using ArmyAI;
using UnityEngine;

namespace PlayerLogic
{
    public partial class Player
    {
        public float GetPlayerPosition() { return transform.position.x; }
        private void OnTriggerEnter2D(Collider2D other)
        {
            var tag = other.gameObject.tag;
            switch (tag)
            {
                case "Building":
                    _activeOrder = PlayerOrders.Orders.BuildOrder;
                    _orderTarget = other.gameObject;
                    break;
                case "Defensive":
                    _activeOrder = PlayerOrders.Orders.DefendOrder;
                    _orderTarget = other.gameObject;
                    break;
                case "Farm":
                    _activeOrder = PlayerOrders.Orders.FarmOrder;
                    _orderTarget = other.gameObject;
                    break;
                case "Pickable":
                    _activeOrder = PlayerOrders.Orders.PickUpOrder;
                    _orderTarget = other.gameObject;
                    break;
                case "Gatherable":
                    _activeOrder = PlayerOrders.Orders.GatherOrder;
                    _orderTarget = other.gameObject;
                    break;
                case "Unit":
                    //We shouldn't set orderTarget to a unit
                    return;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            // var tag = other.gameObject.tag;
            // switch (tag)
            // {
            //     case "Building":
            //     case "Defensive":
            //     case "Farm":
            //     case "Pickable":
            //         break;
            //     case "Unit":
            //         //We shouldn't set orderTarget to a unit
            //         return;
            // }
            // _activeOrder = PlayerOrders.Orders.NoOrder;
        }
        
        private void OnCallKeyPressed()
        {
#if UNITY_EDITOR
            DrawDebug();
#endif
            _animator.SetTrigger(_callVillagerAnimHash);
            //Finding villagers in front of player
            List<Collider2D> objectsInArea = new List<Collider2D>();
            Vector2 pointA = transform.position;
            Vector2 pointB = new Vector2(pointA.x + (LookingDirection * playerData.summonRange), pointA.y + 1);
            Physics2D.OverlapArea(pointA, pointB, new ContactFilter2D().NoFilter(), objectsInArea);
            foreach (var coll2D in objectsInArea)
            {
                if (!coll2D.CompareTag("Unit")) continue;
                //TODO::
                //Need to check if to attack or follow. Should follow some rules. Implement later
                _armyController.AddFollow(coll2D.gameObject);
            }
        }

        public void AddUnit(GameObject unit)
        {
            _armyController.Add(unit);
            _armyController.UpdatePlayerDirection(LookingDirection);
            _armyController.UpdateIsPlayerMoving(_movePressed);
            _armyController.UpdateIsPlayerRunning(_runPressed);
        }

        private void OnInteractFirstPressed()
        {
            //Check if commands can be executed
            switch (_activeOrder)
            {
                case PlayerOrders.Orders.BuildOrder:
                    var building = _orderTarget.transform.parent.parent.GetComponent<BuildingController>();
                    if (building.CanUpgrade())
                    {
                        #if UNITY_EDITOR
                        _orderDebugString = "Loading...";
                        #endif
                    }
                    else
                    {
                        #if UNITY_EDITOR
                        _orderDebugString = "Can't Upgrade";
                        #endif
                    }
                    break;
                case PlayerOrders.Orders.GatherOrder:
                    var bush = _orderTarget.transform.parent.parent.GetComponent<BuildingController>();
                    if (bush.CanBeGathered())
                    {
                        _orderDebugString = "Player ordered Gather in FirstPress State";
                        Debug.Log("Player ordered Gather in FirstPress State");
                        _armyController.Order(PlayerOrders.Orders.GatherOrder,_orderTarget.transform.parent.parent.gameObject);
                    }
                    break;
            }
        }

        private void OnInteractLongPress()
        {
            switch (_activeOrder)
            {
                case PlayerOrders.Orders.BuildOrder:
                    var target = _orderTarget.transform.parent.parent.gameObject;
                    if (!target.GetComponent<BuildingController>().CanUpgrade()) return;
                    
                    if (_armyController.CanExecute(_activeOrder) && _wallet.MakePayment(_activeOrder, target))
                    {
                        _orderDebugString = "Player ordered Build in LongPress Build state";
                        Debug.Log("Player ordered Build in LongPress Build state");
                        _armyController.Order(_activeOrder, target);
                    }
                    else
                    {
                        //TODO::Sound not enough money?
                    }
                    break;
                case PlayerOrders.Orders.RepairOrder:
                    var targetR = _orderTarget.transform.parent.parent.gameObject;
                    if (!targetR.GetComponent<BuildingController>().CanUpgrade()) return;
                    
                    if (_armyController.CanExecute(_activeOrder) && _wallet.MakePayment(_activeOrder, targetR))
                    {
                        _orderDebugString = "Player ordered Build in LongPress Build state";
                        Debug.Log("Player ordered Build in LongPress Build state");
                        _armyController.Order(_activeOrder, targetR);
                    }
                    else
                    {
                        //TODO::Sound not enough money?
                    }
                    break;
            }
        }

        private void OnInteractQuickPress()
        {
            switch (_activeOrder)
            {
                case PlayerOrders.Orders.PickUpOrder:
                    _orderDebugString = "Player ordered Pickup in QuickPress Pickup state";
                    Debug.Log("Player ordered Pickup in QuickPress Pickup state");
                    var pickable = _orderTarget.GetComponent<Pickable>();
                    if (!pickable.HasPickable()) return;
                    _armyController.Order(PlayerOrders.Orders.PickUpOrder, _orderTarget);
                    return;
                case PlayerOrders.Orders.BuildOrder:
                    var building = _orderTarget.transform.parent.parent.GetComponent<BuildingController>();
                    if (building.IsUpgrading())
                    {
                        _orderDebugString = "Player ordered Build in QuickPress Build state"; 
                        Debug.Log("Player ordered Build in QuickPress Build state");
                        _armyController.Order(PlayerOrders.Orders.BuildOrder, building.gameObject);
                    }
                    else
                    {
                        if (!building.HasPickable()) return;
                        _orderDebugString = "Player ordered Pickup in QuickPress Build state"; 
                        Debug.Log("Player ordered Pickup in QuickPress Build state");
                        _armyController.Order(PlayerOrders.Orders.PickUpOrder, building.gameObject.transform.GetChild(0).gameObject);
                    }
                    return;
                case PlayerOrders.Orders.RepairOrder:
                    var buildingR = _orderTarget.transform.parent.parent.GetComponent<BuildingController>();
                    if (buildingR.IsUpgrading())
                    {
                        _orderDebugString = "Player ordered Repair in QuickPress Repair state"; 
                        Debug.Log("Player ordered Repair in QuickPress Repair state");
                        _armyController.Order(PlayerOrders.Orders.BuildOrder, buildingR.gameObject);
                    }
                    break;
            }
        }

        public void AddMoney(int amount)
        {
            _wallet.AddMoney(amount);
        }

        private GameObject _orderTarget;
        private ArmyController _armyController;
        private PlayerOrders.Orders _activeOrder = PlayerOrders.Orders.FollowOrder;
        private Wallet _wallet;

#if UNITY_EDITOR
        void DrawDebug()
        {
            Vector2 position = transform.position;
            Debug.DrawLine(new Vector3(position.x, position.y,0), new Vector3(position.x + playerData.summonRange * LookingDirection, position.y + 1,0), Color.black, 5f);
        }
        private IEnumerator DrawSummonEffect()
        {
            for (float i = 0; i < 50; ++i)
            {
                float positionX = transform.position.x;
                Debug.DrawLine(new Vector3(positionX, 0,0), new Vector3(positionX + playerData.summonRange * LookingDirection, 1,0), Color.black);
                yield return null;
            }
        }
#endif
        private string _orderDebugString;
        private void OnGUI()
        {
            string target = _orderTarget != null ? _orderTarget.name : "no current state";
            string order = _activeOrder.ToString();
            GUILayout.Label($"<color='black><size=40>{target}->{order}</size></color>\n" +
                            $"<color='black><size=40>{_orderDebugString}</size></color>\n");
        }
    }
}
