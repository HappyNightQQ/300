using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

namespace PlayerLogic
{
    public class AnimationSpeedUsingFloat : StateMachineBehaviour
    {
        private Player player = null;
        public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
        {
            player = animator.gameObject.GetComponent<Player>();
        }

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            player = animator.gameObject.GetComponent<Player>();
        }

        private int i = 0;
        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.speed = Mathf.Abs(player.MovingDirection);
        }
    }
}