using System;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [SerializeField]private GameObject targetToBeFollowed = null;
    
    private void Start()
    {
        if (targetToBeFollowed == null)
        {
            targetToBeFollowed = GameObject.FindWithTag("Player");
        }
    }
    
    private void LateUpdate()
    {
        FollowPlayer();
    }

    private void FollowPlayer()
    {
        var goPosition = transform.position;
        if (goPosition.x.Equals(targetToBeFollowed.transform.position.x))
            return;

        var direction = targetToBeFollowed.transform.position.x - goPosition.x;

        transform.position += new Vector3(direction * Time.deltaTime, 0, 0);
    }
}
